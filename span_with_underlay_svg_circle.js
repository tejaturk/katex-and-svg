// Underlay KaTeX with svg
const div = d3.select("#fig");
const span = div.select("#node-text"); // (Step 1)

const width = 50;
const height = 50;
const r = 25;

// Create the objects
// container (Step 2)
var container = span.append("span")
  .style("display", "inline-block"); // make span possible to contain svg

// svg (Step 3)
const svg = container.append("svg")
  .attr("width", width)
  .attr("height", height);

// g
const g = svg.append("g")
  .attr("id", "node");

// circle
const circle = g.append("circle")
  .attr("cx", width/2)
  .attr("cy", height/2)
  .attr("r", r)
  .attr("fill", "#faddcd");

// Change the position of the span containing the katex to relative such that the container can be put under it
span.style("position", "relative");
// Likewise the position of the container within span should be absolute and z-index set to -1 for container to be in the background
container.style("position", "absolute")
  .style("z-index", -1);

  // Get the positions of the crucial elements (Step 4)

  //span
  const span_dim = span.node().getBoundingClientRect();
  const span_left = span_dim.left;
  const span_top = span_dim.top;

  // container
  var container_dim = container.node().getBoundingClientRect();
  var container_left = container_dim.left;
  var container_top = container_dim.top;

  // g
  const g_dim = g.node().getBoundingClientRect();
  const g_left = g_dim.left;
  const g_top = g_dim.top;
  const g_width = g_dim.width;
  const g_height = g_dim.height;

  // Get the positions of the node element within container:
  const delta_g_left = g_left - container_left;
  const delta_g_top = g_top - container_top;

// WAIT until KaTeX renders all the math formulas (and then calculate the new positions of the container object)
window.addEventListener("load", function(){
  // Get actual dimensions of the katex text (considering the baseline) to put it in the middle
  const tex = span.select("span.katex");
  const tex_dim = tex.node().getBoundingClientRect();
  const tex_left = tex_dim.left;
  const tex_top = tex_dim.top;
  const tex_width = tex_dim.width;
  const tex_height = tex_dim.height;

  // Get the positions of the tex element within the span
  const delta_tex_left = tex_left - span_left;
  const delta_tex_top = tex_top - span_top;

  // Set the position of the container holding the svg and the node
  container.style("left", delta_tex_left + tex_width/2 - delta_g_left - g_width/2 + "px");
  container.style("top", delta_tex_top + tex_height/2 - delta_g_top - g_height/2 + "px");

});
