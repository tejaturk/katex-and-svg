// Add KaTeX to svg within a foreignObject
const div = d3.select("#fig");

const width = 50;
const height = 50;
const r = 25;

// Create the objects
// svg (Step 1)
const svg = div.append("svg")
  .attr("width", width)
  .attr("height", height);

// g (Step 2)
const g = svg.append("g")
  .attr("id", "node");

// circle within g (Step 3)
const circle = g.append("circle")
  .attr("cx", width/2)
  .attr("cy", height/2)
  .attr("r", r)
  .attr("fill", "#faddcd");

// foreignObject to contain KaTeX (Step 4)
var foreignObject = svg.append("foreignObject")
  .attr("width", 1)
  .attr("height", 1);

// span within foreignObject to add KaTeX (mind the xhtml: before span!) (Step 5)
const span = foreignObject.append("xhtml:span")
  .attr("id", "node-text")
  .html("\\(\\lambda x^2\\)");

// Get the positions of the crucial elements (Step 6)
// svg
const svg_dim = svg.node().getBoundingClientRect();
const svg_left = svg_dim.left;
const svg_top = svg_dim.top;

// g
const g_dim = g.node().getBoundingClientRect();
const g_left = g_dim.left;
const g_top = g_dim.top;
const g_width = g_dim.width;
const g_height = g_dim.height;

// foreignObject
var foreignObject_dim = foreignObject.node().getBoundingClientRect();
var foreignObject_left = foreignObject_dim.left;
var foreignObject_top = foreignObject_dim.top;

// Get the positions of the node element within svg:
const delta_g_left = g_left - svg_left;
const delta_g_top = g_top - svg_top;

// WAIT until KaTeX renders all the math formulas (and then calculate the new positions of the foreign object)
window.addEventListener("load", function(){
  // Get actual dimensions of the katex text (considering the baseline) to put it in the middle
  const tex = span.select("span.katex");
  const tex_dim = tex.node().getBoundingClientRect();
  const tex_left = tex_dim.left;
  const tex_top = tex_dim.top;
  const tex_width = tex_dim.width;
  const tex_height = tex_dim.height;

  // Get the positions of the tex element within the foreignObject
  const delta_tex_left = tex_left - foreignObject_left;
  const delta_tex_top = tex_top - foreignObject_top;

  // Set the position and the size of the foreign object holding the formula
  foreignObject.attr("x", delta_g_left + g_width/2 - delta_tex_left - tex_width/2);
  foreignObject.attr("y", delta_g_top + g_height/2 - delta_tex_top - tex_height/2);
  foreignObject.attr("width", tex_width);
  foreignObject.attr("height", tex_height);

});
