# KaTeX, svg and D3.js

This is a GitLab repository accompanying the KaTeX, svg and D3.js blog post from [www.tejaturk.com/posts/katex_svg_d3/](https://www.tejaturk.com/posts/katex_svg_d3/). Briefly, it presents three possibilities on how to use mathematical notation with D3.js or with svg in general. More details are provided in the blog post.
