// Overlay svg with KaTeX
const div = d3.select("#fig");

const width = 50;
const height = 50;
const r = 25;

// Create the objects
// svg (Step 1)
const svg = div.append("svg")
  .attr("width", width)
  .attr("height", height);

// g (Step 2)
const g = svg.append("g")
  .attr("id", "node");

// circle
const circle = g.append("circle")
  .attr("cx", width/2)
  .attr("cy", height/2)
  .attr("r", r)
  .attr("fill", "#faddcd");

// span (Step 3)
var span = div.append("span")
  .attr("id", "node-text")
  .html("\\(\\lambda x^2\\)");

// Change the position of the div containing the svg to relative such that the span can be put over it
div.style("position", "relative");
// Likewise the position of the span within the div should be absolute
span.style("position", "absolute");

// Get the positions of the crucial elements (Step 4)

//div
const div_dim = div.node().getBoundingClientRect();
const div_left = div_dim.left;
const div_top = div_dim.top;

// g
const g_dim = g.node().getBoundingClientRect();
const g_left = g_dim.left;
const g_top = g_dim.top;
const g_width = g_dim.width;
const g_height = g_dim.height;

// span
var span_dim = span.node().getBoundingClientRect();
var span_left = span_dim.left;
var span_top = span_dim.top;

// Get the positions of the node element within div:
const delta_g_left = g_left - div_left;
const delta_g_top = g_top - div_top;

// WAIT until KaTeX renders all the math formulas (and then calculate the new positions of the span object)
window.addEventListener("load", function(){
  // Get actual dimensions of the katex text (considering the baseline) to put it in the middle
  const tex = span.select("span.katex");
  const tex_dim = tex.node().getBoundingClientRect();
  const tex_left = tex_dim.left;
  const tex_top = tex_dim.top;
  const tex_width = tex_dim.width;
  const tex_height = tex_dim.height;

  // Get the positions of the tex element within the span
  const delta_tex_left = tex_left - span_left;
  const delta_tex_top = tex_top - span_top;

  // Set the position of the span holding the formula
  span.style("left", delta_g_left + g_width/2 - delta_tex_left - tex_width/2 + "px");
  span.style("top", delta_g_top + g_height/2 - delta_tex_top - tex_height/2 + "px");

});
